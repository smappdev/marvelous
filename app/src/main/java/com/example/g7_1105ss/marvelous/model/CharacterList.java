package com.example.g7_1105ss.marvelous.model;


import java.util.LinkedList;
import java.util.List;

public class CharacterList {

    private List<Character> characters = new LinkedList<>();

    public List<Character> getCharacters() {
        return characters;
    }

    public void setCharacters(List<Character> characters) {
        this.characters = characters;
    }
}
