package com.example.g7_1105ss.marvelous.model.entities;


import com.google.gson.annotations.SerializedName;

public class CharacterResponseEntity {

    @SerializedName("data") CharacterDataEntity data;

    public CharacterDataEntity getData() {
        return data;
    }

    public void setData(CharacterDataEntity data) {
        this.data = data;
    }
}
