package com.example.g7_1105ss.marvelous.model.json;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class CharacterDataResponseEntity {

    private @SerializedName("offset") int offset;
    private @SerializedName("limit") int limit;
    private @SerializedName("total") int total;
    private @SerializedName("count") int count;
    private @SerializedName("results") List<CharacterEntity> results;

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<CharacterEntity> getResults() {
        return results;
    }

    public void setResults(List<CharacterEntity> results) {
        this.results = results;
    }
}
