package com.example.g7_1105ss.marvelous.utils;

import com.example.g7_1105ss.marvelous.config.MarvelApiKeys;

import java.util.UUID;

public class MarvelAPIHashGenerator {

    public static String newHash(String timeStamp) {
        String textToCypher = timeStamp + MarvelApiKeys.PRIVATE_KEY + MarvelApiKeys.PUBLIC_KEY;
        return MD5Generator.generateMD5(textToCypher);
    }

}
