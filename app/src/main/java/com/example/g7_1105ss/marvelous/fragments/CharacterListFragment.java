package com.example.g7_1105ss.marvelous.fragments;


import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.g7_1105ss.marvelous.R;
import com.example.g7_1105ss.marvelous.adapters.CharactersAdapter;
import com.example.g7_1105ss.marvelous.config.MarvelApiKeys;
import com.example.g7_1105ss.marvelous.managers.EndlessRecyclerViewScrollListener;
import com.example.g7_1105ss.marvelous.managers.MarvelAPI;
import com.example.g7_1105ss.marvelous.model.Character;
import com.example.g7_1105ss.marvelous.model.CharacterList;
import com.example.g7_1105ss.marvelous.model.entities.CharacterResponseEntity;
import com.example.g7_1105ss.marvelous.model.entities.CharacterResultsEntity;
import com.example.g7_1105ss.marvelous.utils.MarvelAPIHashGenerator;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * A simple {@link Fragment} subclass.
 */
public class CharacterListFragment extends Fragment {

    private RecyclerView charactersRecyclerView;
    private CharactersAdapter adapter;
    private CharacterList characters;
    private EndlessRecyclerViewScrollListener scrollListener;
    final MarvelAPI m = new MarvelAPI();

    public CharacterListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_character_list, container, false);

        charactersRecyclerView = (RecyclerView) view.findViewById(R.id.fragment_character_list_recyclerview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        charactersRecyclerView.setLayoutManager(linearLayoutManager);

        characters = new CharacterList();

        m.setAdapter(characters, getContext());
        m.getAllSuperHeroes(view.getContext(), charactersRecyclerView);

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadNextDataDromApi(page, m);
            }
        };
        charactersRecyclerView.addOnScrollListener(scrollListener);

        setHasOptionsMenu(true);
        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                apiSearchByName(query, m);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                apiSearchByName(newText, m);
                return false;
            }
        });


        super.onCreateOptionsMenu(menu, inflater);
    }


    private void loadNextDataDromApi(int page, MarvelAPI m) {
        if (m.getName().equals("")) {

            m.setOffset("&offset=" + (page * 10));
            String newURL = m.getApiUrl().replaceFirst("(.*)&offset=(.*)", "");
            m.setApiUrl(newURL + m.getOffset());
            m.getAllSuperHeroes(getContext(), charactersRecyclerView);
            m.setApiUrl(newURL);
        }else{
            return;
        }
    }

    private void apiSearchByName(String queryName, MarvelAPI m){
        System.out.println("BUSCANDO EN LA API..." + queryName);

        if(queryName.equals("")) {
            m.setName("");
        }else{
            m.setName("&name=" + queryName);
        }

        String newURL = m.getApiUrl().replaceFirst("(.*)&name=(.*)", "");
        m.setApiUrl(newURL + m.getName());

        m.setCharacterList(new CharacterList());
        m.setAdapter(m.getCharacterList(), getContext());
        m.getAllSuperHeroes(getContext(), charactersRecyclerView);
        m.setApiUrl(newURL);
    }

    private static final String BUNDLE_RECYCLER_LAYOUT = "classname.recycler.layout";

    /**
     * This is a method for Fragment.
     * You can do the same in onCreate or onRestoreInstanceState
     */
    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        if(savedInstanceState != null)
        {
            Parcelable savedRecyclerLayoutState = savedInstanceState.getParcelable(BUNDLE_RECYCLER_LAYOUT);
            charactersRecyclerView.getLayoutManager().onRestoreInstanceState(savedRecyclerLayoutState);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(BUNDLE_RECYCLER_LAYOUT, charactersRecyclerView.getLayoutManager().onSaveInstanceState());
    }
}
