package com.example.g7_1105ss.marvelous.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.g7_1105ss.marvelous.adapters.CharactersAdapter;
import com.example.g7_1105ss.marvelous.config.MarvelApiKeys;
import com.example.g7_1105ss.marvelous.fragments.CharacterListFragment;
import com.example.g7_1105ss.marvelous.model.Character;
import com.example.g7_1105ss.marvelous.model.CharacterList;
import com.example.g7_1105ss.marvelous.model.entities.CharacterDataEntity;
import com.example.g7_1105ss.marvelous.model.entities.CharacterResponseEntity;
import com.example.g7_1105ss.marvelous.model.entities.CharacterResultsEntity;
import com.example.g7_1105ss.marvelous.utils.MarvelAPIHashGenerator;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Downloader;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;

public class MarvelAPI {

    // https://gateway.marvel.com:443/v1/public/characters?apikey=a6db90ab56d48965159b1095a702d53b

    public static final String CHARACTERS_URL = "https://gateway.marvel.com:443/v1/public/characters?";

    String timeStamp = UUID.randomUUID().toString();
    String apiUrl = CHARACTERS_URL
            + "apikey=" + MarvelApiKeys.PUBLIC_KEY
            + "&ts=" + timeStamp
            + "&hash=" + MarvelAPIHashGenerator.newHash(timeStamp)
            + "&limit=10";
    CharacterList characterList = new CharacterList();
    String offset = "&offset=0";
    String name = "";
    //
    CharactersAdapter adapter;

    public void getAllSuperHeroes(final Context context, final RecyclerView charactersRecyclerView){
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest request = new StringRequest(apiUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("RESPONSE", response);

                Reader reader = new StringReader(response);
                Gson gson = new GsonBuilder().create();

                CharacterResponseEntity characterResponse = gson.fromJson(reader, CharacterResponseEntity.class);
                List<CharacterResultsEntity> parseResult = characterResponse.getData().getResults();

                for (CharacterResultsEntity result : parseResult) {

                    Character newCharacter = new Character();

                    newCharacter.setName(result.getName());
                    if (result.getDescription().equals("")){
                        newCharacter.setDescription("No description available...");
                    }else {
                        newCharacter.setDescription(result.getDescription());
                    }
                    newCharacter.setThumbnail(result.getThumbnail().getPath() + "." + result.getThumbnail().getExtension());

                    characterList.getCharacters().add(newCharacter);
                    System.out.println(newCharacter.getDescription());
                    //System.out.println(result.getThumbnail().getPath() + "." + result.getThumbnail().getExtension());
                }
                //CharactersAdapter adapter = new CharactersAdapter(characterList, context);
                int lastFirstVisiblePosition = ((LinearLayoutManager) charactersRecyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
                charactersRecyclerView.setAdapter(adapter);
                (charactersRecyclerView.getLayoutManager()).scrollToPosition(lastFirstVisiblePosition);

                //adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(request);
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CharacterList getCharacterList() {
        return characterList;
    }

    public void setCharacterList(CharacterList characterList) {
        this.characterList = characterList;
    }

    //

    public CharactersAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(CharacterList characterList, Context context) {
        this.adapter = new CharactersAdapter(this.characterList,context);
    }

    //


}
