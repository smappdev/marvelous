package com.example.g7_1105ss.marvelous;

import android.test.AndroidTestCase;

import com.example.g7_1105ss.marvelous.utils.MD5Generator;

public class MD5GeneratorTests extends AndroidTestCase {

    public static final int CORRECT_MD5_HASH_LENGTH = 32;

    public void testMD5GeneratorReturnsNullWhenIPassNull(){
        String sut = MD5Generator.generateMD5(null);
        assertNull(sut);
    }
    public void testMD5GeneratorReturnsNullWhenIPassEmpty(){
        String sut = MD5Generator.generateMD5("");
        assertNull(sut);
    }
    public void testMD5GeneratorReturnsValueHashWhenIPassA(){
        String sut = MD5Generator.generateMD5("a");
        assertEquals(sut, "0cc175b9c0f1b6a831c399e269772661");
        assertEquals(sut.length(), CORRECT_MD5_HASH_LENGTH);
    }

    public void testMD5GeneratorReturnsValueHashWhenIPassB(){
        String sut = MD5Generator.generateMD5("b");
        assertEquals(sut, "92eb5ffee6ae2fec3ad71c777531578f");
    }
}
