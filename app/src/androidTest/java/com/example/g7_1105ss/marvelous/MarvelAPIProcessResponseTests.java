package com.example.g7_1105ss.marvelous;


import android.test.AndroidTestCase;

import com.example.g7_1105ss.marvelous.managers.MarvelAPI;
import com.example.g7_1105ss.marvelous.model.json.CharacterResponseEntity;

public class MarvelAPIProcessResponseTests extends AndroidTestCase{

    public void test_ProcessResponse_ReturnsNull_WhenPassedNull(){
        MarvelAPI sut = new MarvelAPI();
        CharacterResponseEntity characterResponseEntity = sut.processResponse(null);
        assertNull(characterResponseEntity);
    }

    public void test_ProcessResponse_ReturnsNull_WhenPassedEmptyString(){
        MarvelAPI sut = new MarvelAPI();
        CharacterResponseEntity characterResponseEntity = sut.processResponse("");
        assertNull(characterResponseEntity);
    }

    public void test_ProcessResponse_ReturnsNull_WhenPassedUnterminatedResponse(){
        MarvelAPI sut = new MarvelAPI();
        CharacterResponseEntity characterResponseEntity = sut.processResponse("{\"code\": 200,}");
        assertNull(characterResponseEntity);
    }

}
